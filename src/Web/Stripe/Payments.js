"use strict";

// Stripe -> HTMLElement -> (String -> Foreign -> Eff e Unit) -> Eff e Unit
exports._createPayment = function(Stripe, form, cb) {
    return function() {
        Stripe.card.createToken(form, function(status, response) {
            cb(status, response)();
        });
    };
};

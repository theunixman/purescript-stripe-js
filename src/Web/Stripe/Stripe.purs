module Web.Stripe.Stripe where

-- | The source URL for the Stripe JS bundle.
stripeSourceURL ∷ String
stripeSourceURL = "https://js.stripe.com/v2/"

module Web.Stripe.Payments where

import Control.Monad.Eff
import DOM (DOM)
import DOM.HTML.Types (HTMLElement, HTMLFormElement)
import Data.Function.Uncurried
import Data.Foreign
import Data.Either
import Web.Stripe.Types

-- | The createToken function.
foreign import _createToken ∷
    ∀ e.
    Fn3 Stripe HTMLElement (String → Foreign → Eff (stripe ∷ STRIPE | e) Unit) Eff (stripe ∷ STRIPE | e) Unit

loadToken ∷ Foreign → F (Either ChargeTokenError ChargeToken)
loadToken resp =
    case read resp of
        Left e → Left e
        Right resp → Right $ case resp of
            r@(ChargeTokenError{}) → Left r
            t@(ChargeToken{}) → Right t

createToken ∷
    ∀ e.
    Stripe →
    HTMLFormElement →
    Eff (stripe ∷ STRIPE | e) ChargeToken
createToken s f =
    let
        el = htmlFormElemenToHTMLElement form
        cb succ _ resp = succ $ case loadToken resp of
            Left e → error $ show e
            Right l@Left _ → Left l
            Right r@Right _ → Right r
        cct = callFn3 s el cb
    in
        makeAff (\_ succ → cb succ <$> cct)

module Web.Stripe.Types.Errors where

import Prelude
import Web.Stripe.Types.Types
import Data.Either
import Data.Generic
import Data.Foreign
import Data.Foreign.Class
import Data.Maybe

data StripeErrorType =
    APIConnectionError |
    APIError |
    AuthenticationError |
    CardError |
    InvalidRequestError |
    RateLimitError
derive instance genericStripeErrorType ∷ Generic StripeErrorType
derive instance equalStripeErrorType ∷ Eq StripeErrorType
derive instance ordStripeErrorType ∷ Ord StripeErrorType
instance showStripeErrorType ∷ Show StripeErrorType where
    show = gShow
instance isForeignStripeErrorType ∷ IsForeign StripeErrorType where
    read f = case readString f of
        Left e → Left e
        Right t → case t of
            "api_connection_error" → Right APIConnectionError
            "api_error" → Right APIError
            "authentication_error" → Right AuthenticationError
            "card_error" → Right CardError
            "invalid_request_error" → Right InvalidRequestError
            "rate_limit_error" → Right RateLimitError
            s → Left $ TypeMismatch "StripeErrorType unknown" s
data StripeErrorCode =
    -- | The card number is not a valid credit card number.
    InvalidNumber |
    -- | The Card'S Expiration Month Is Invalid.
    InvalidExpiryMonth |
    -- | The Card'S Expiration Year Is Invalid.
    InvalidExpiryYear |
    -- | The Card'S Security Code Is Invalid.
    InvalidCvc |
    -- | The Card Number Is Incorrect.
    IncorrectNumber |
    -- | The Card Has Expired.
    ExpiredCard |
    -- | The Card'S Security Code Is Incorrect.
    IncorrectCvc |
    -- | The Card'S Zip Code Failed Validation.
    IncorrectZip |
    -- | The Card Was Declined.
    CardDeclined |
    -- | There Is No Card On A Customer That Is Being Charged.
    Missing |
    -- | An Error Occurred While Processing The Card.
    ProcessingError

derive instance genericStripeErrorCode ∷ Generic StripeErrorCode
instance showStripeErrorCode ∷ Show StripeErrorCode where
    show = gShow
derive instance eqStripeErrorCode ∷ Eq StripeErrorCode
derive instance ordStripeErrorCode ∷ Ord StripeErrorCode
instance isForeignStripeErrorCode ∷ IsForeign StripeErrorCode where
    read f = case readString f of
        Left e → Left e
        Right t → case t of
            "invalid_number" → Right InvalidNumber
            "invalid_expiry_month" → Right InvalidExpiryMonth
            "invalid_expiry_year" → Right InvalidExpiryYear
            "invalid_cvc" → Right InvalidCvc
            "incorrect_number" → Right IncorrectNumber
            "expired_card" → Right ExpiredCard
            "incorrect_cvc" → Right IncorrectCvc
            "incorrect_zip" → Right IncorrectZip
            "card_declined" → Right CardDeclined
            "missing" → Right Missing
            "processing_error" → Right ProcessingError
            s → Left $ TypeMismatch "StripeErrorCode unknown" s

data ChargeTokenError = ChargeTokenError {
    etype ∷ StripeErrorType,
    code ∷ StripeErrorCode,
    message ∷ String,
    param ∷ Maybe String
    }

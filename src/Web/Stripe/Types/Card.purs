module Web.Stripe.Types.Card where

import Web.Stripe.Types.Types
import Data.DateTime.Instant
import Web.Stripe.Types.Address
import Data.Date
import Data.Date.Component
import Data.Enum

data Card = Card Name Address Last4 ExpirationDate Brand Funding
derive instance genericCard ∷ Generic Card
instance showCard ∷ Show Card where
    show = gShow
instance eqCard ∷ Eq Card where
    eq = gEq
instance ordCard ∷ Ord Card where
    compare = gCompare
instance cardIsForeign ∷ IsForeign Card where
    read value = Card
                 <$> readProp "name" value
                 <*> gRead value
                 <*> gRead value
                 <*> readProp "last4" value
                 <*> readProp "brand" value
                 <*> readProp "funding" value

newtype Name = Name String
derive instance genericName ∷ Generic Name
instance showName ∷ Show Name where
    show = gShow
instance eqName ∷ Eq Name where
    eq = gEq
instance ordName ∷ Ord Name where
    compare = gCompare
instance nameIsForeign ∷ IsForeign Name where
    read = gRead

newtype ExpirationDate = ExpirationDate Date
derive instance genericExpirationDate ∷ Generic ExpirationDate
instance showExpirationDate ∷ Show ExpirationDate where
    show = gShow
instance eqExpirationDate ∷ Eq ExpirationDate where
    eq = gEq
instance ordExpirationDate ∷ Ord ExpirationDate where
    compare = gCompare
instance expirationDateIsForeign ∷ IsForeign ExpirationDate where
    read value = ExpirationDate <$>
                 (canonicalDate
                  <$> (readProp "exp_year" value >>= readYear)
                  <*> (readProp "exp_month" value >>= readMonth)
                  <*> pure bottom)

readYear ∷ Foreign → Either ForeignError Year
readYear value =
    readInt value >>=
    readMaybe toEnum "Expected Year between -271820  275759."

readMonth ∷ Foreign → Either ForeignError Month
readMonth value =
    readInt value >>=
    readMaybe toEnum "Expected Month between 1 and 12."

newtype Last4 = Last4 Number
derive instance genericLast4 ∷ Generic Last4
instance showLast4 ∷ Show Last4 where
    show = gShow
instance eqLast4 ∷ Eq Last4 where
    eq = gEq
instance ordLast4 ∷ Ord Last4 where
    compare = gCompare

newtype Brand = Brand String
derive instance genericBrand ∷ Generic Brand
instance showBrand ∷ Show Brand where
    show = gShow
instance eqBrand ∷ Eq Brand where
    eq = gEq
instance ordBrand ∷ Ord Brand where
    compare = gCompare
instance isForeignBrand ∷ IsForeign Brand where
    read = gRead

newtype Funding = Funding String
derive instance genericFunding ∷ Generic Funding
instance showFunding ∷ Show Funding where
    show = gShow
instance eqFunding ∷ Eq Funding where
    eq = gEq
instance ordFunding ∷ Ord Funding where
    compare = gCompare
instance isForeignFunding ∷ IsForeign Funding where
    read = gRead

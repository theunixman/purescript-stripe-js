module Web.Stripe.Types.Types where

import Prelude
import Data.Foreign
import Data.Foreign.Generic (Options, defaultOptions, readGeneric)
import Data.Generic (class Generic, gEq, gShow, gCompare)
import Data.Maybe
import Data.Either

-- | The Effect type of stripe functions.
foreign import data STRIPE ∷ !

foreign import data Stripe ∷ *

opts :: Options
opts = defaultOptions { unwrapNewtypes = true, tupleAsArray = true }

gRead ∷ ∀ a. (Generic a) ⇒ Foreign → F a
gRead = readGeneric opts

-- | Adaptor for a 'Maybe' to an 'F'.
-- |
--| Will read an 'a', and change the 'Maybe b' to a 'TypeMismatch' with the message and the 'show' of 'a'.
readMaybe ∷ ∀ a b. Show a ⇒ (a → Maybe b) → String → a → F b
readMaybe r e a =
    let
        err = TypeMismatch e ("Got: " <> show a)
        m = r a
    in
        maybe (Left err) Right m

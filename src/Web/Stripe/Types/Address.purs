module Web.Stripe.Types.Address where

import Prelude
import Web.Stripe.Types.Types
import Data.Generic
import Data.Foreign
import Data.Foreign.Class

data Address = Address AddressLine AddressLine City State ZipCode Country
derive instance genericAddress ∷ Generic Address
derive instance eqAddress ∷ Eq Address
derive instance ordAddress ∷ Ord Address
instance isForeignAddress ∷ IsForeign Address where
    read value = Address
                 <$> readProp "address_line1" value
                 <*> readProp "address_line2" value
                 <*> readProp "address_city" value
                 <*> readProp "address_state" value
                 <*> readProp "address_zip" value
                 <*> readProp "address_country" value

newtype AddressLine = AddressLine String
derive instance genericAddressLine ∷ Generic AddressLine
instance showAddressLine ∷ Show AddressLine where
    show = gShow
derive instance eqAddressLine ∷ Eq AddressLine
derive instance ordAddressLine ∷ Ord AddressLine
instance isForeignAddressLine ∷ IsForeign AddressLine where
    read = gRead

newtype City = City String
derive instance genericCity ∷ Generic City
instance showCity ∷ Show City where
    show = gShow
derive instance eqCity ∷ Eq City
derive instance ordCity ∷ Ord City
derive instance isForeignCity ∷ IsForeign City

newtype State = State String
derive instance genericState ∷ Generic State
instance showState ∷ Show State where
    show = gShow
derive instance eqState ∷ Eq State
derive instance ordState ∷ Ord State
derive instance isForeignState ∷ IsForeign State

newtype ZipCode = ZipCode String
derive instance genericZipCode ∷ Generic ZipCode
instance showZipCode ∷ Show ZipCode where
    show = gShow
derive instance eqZipCode ∷ Eq ZipCode
derive instance ordZipCode ∷ Ord ZipCode
derive instance isForeignZipCode ∷ IsForeign ZipCode

newtype Country = Country String
derive instance genericCountry ∷ Generic Country
instance showCountry ∷ Show Country where
    show = gShow
derive instance eqCountry ∷ Eq Country
derive instance ordCountry ∷ Ord Country
derive instance isForeignCountry ∷ IsForeign Country

module  Web.Stripe.Types.ChargeToken where

import Web.Stripe.Types.Types (
    class IsForeign, class Ord, class Eq,
    class Show, class Generic, F, gRead,
    gCompare, gEq, gShow, readMaybe, pure,
    readNumber, readProp, (<$>), (>>=), (<*>))
import Data.DateTime.Instant (Instant, instant)
import Data.Time.Duration (Milliseconds, Seconds(..), convertDuration)

import Web.Stripe.Types.Card (Card)

data ChargeToken = ChargeToken Token Card CreatedAt LiveMode ChargeType Used
derive instance genericChargeToken ∷ Generic ChargeToken
instance showChargeToken ∷ Show ChargeToken where
    show = gShow
instance eqChargeToken ∷ Eq ChargeToken where
    eq = gEq
instance ordChargeToken ∷ Ord ChargeToken where
    compare = gCompare
instance isForeignChargeToken ∷ IsForeign ChargeToken where
    read value = ChargeToken
                 <$> readProp "id" value
                 <*> readProp "card" value
                 <*> readProp "created" value
                 <*> readProp "livemode" value
                 <*> readProp "type" value
                 <*> readProp "used" value

newtype Token = Token String
derive instance genericToken ∷ Generic Token
instance showToken ∷ Show Token where
    show = gShow
instance eqToken ∷ Eq Token where
    eq = gEq
instance ordToken ∷ Ord Token where
    compare = gCompare
instance isForeignToken ∷ IsForeign Token where
    read = gRead

newtype CreatedAt = CreatedAt Instant
derive instance genericCreatedAt ∷ Generic CreatedAt
instance showCreatedAt ∷ Show CreatedAt where
    show = gShow
instance eqCreatedAt ∷ Eq CreatedAt where
    eq = gEq
instance ordCreatedAt ∷ Ord CreatedAt where
    compare = gCompare
instance isForeignCreatedAt ∷ IsForeign CreatedAt where
    read value =
        let
            milliseconds ∷ F Milliseconds
            milliseconds = readNumber value >>= \s → pure (convertDuration (Seconds s))

            inst ∷ F Instant
            inst = milliseconds >>= readMaybe instant "Expecting seconds sinec UNIX Epoch"
        in
            CreatedAt <$> inst

newtype ChargeType = ChargeType String
derive instance genericChargeType ∷ Generic ChargeType
instance showChargeType ∷ Show ChargeType where
    show = gShow
instance eqChargeType ∷ Eq ChargeType where
    eq = gEq
instance ordChargeType ∷ Ord ChargeType where
    compare = gCompare
instance isForeignChargeType ∷ IsForeign ChargeType where
    read = gRead

newtype LiveMode = LiveMode Boolean
derive instance genericLiveMode ∷ Generic LiveMode
instance showLiveMode ∷ Show LiveMode where
    show = gShow
instance eqLiveMode ∷ Eq LiveMode where
    eq = gEq
instance ordLiveMode ∷ Ord LiveMode where
    compare = gCompare
instance isForeignLIveMode ∷ IsForeign LiveMode where
    read = gRead

newtype Used = Used Boolean
derive instance genericUsed ∷ Generic Used
instance showUsed ∷ Show Used where
    show = gShow
instance eqUsed ∷ Eq Used where
    eq = gEq
instance ordUsed ∷ Ord Used where
    compare = gCompare
instance isForeignUsed ∷ IsForeign Used where
    read = gRead
